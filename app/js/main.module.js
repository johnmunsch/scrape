angular.module('Scrape', [
    'ngRoute',
    'ngSanitize',
    'cfp.hotkeys'
  ])
  .config(function($routeProvider) {
    $routeProvider
      .when('/', {
        controller: 'MainController',
        templateUrl: 'views/main.html'
      })
      .otherwise('/');
  });
