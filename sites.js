module.exports = [
  {
    url: 'https://rlsbb.ukbypass.win/page/',
    rssFeed: 'http://feeds.feedburner.com/rlsbb/MrCi',
    name: 'rlsbb',
    scope: '.post',
    schema: {
      title: 'h2 a',
      img: '.entry-content img@src',
      link: 'h2 a[href]',
      description: '.entry-content:nth-child(2)'
    },
    tags: [ 'rlsbb' ]
  },
  {
    url: 'http://2ddl.io/page/',
    rssFeed: 'http://http://twoddl.me/feed/',
    name: '2DDL',
    scope: '.post',
    schema: {
      title: 'h2 a',
      img: 'img@src',
      link: 'h2 a@href',
      description: 'div.description'
    },
    tags: [ '2DDL' ]
  },
  {
    url: 'http://downmagaz.com/page/',
    rssFeed: 'http://feeds.feedburner.com/DownloadMagazine',
    name: 'DownMagaz.CoM',
    scope: '.story',
    schema: {
      title: 'div.sheading span',
      img: 'img@src',
      link: 'div.sheading a@href'
    },
    post: function (item) {
      if (item.img) {
        item.img = item.img.replace('/miniposter/miniposter.php?src=', '');
      }
    },
    tags: [ 'DownMagaz.CoM', 'magazine' ]
  },
  {
    url: 'http://newcomic.info/page/',
    rssFeed: 'http://newcomic.info/rss.xml',
    name: 'Newcomic.info',
    scope: 'article',
    schema: {
      title: 'h3 a',
      img: 'img@src',
      link: 'h3 a@href'
    },
    tags: [ 'Newcomic.info', 'comic book' ]
  },
  {
    url: 'http://www.rlslog.net/page/',
    name: 'Releaselog',
    scope: '.entry',
    schema: {
      title: 'h3',
      img: '.entrybody img@src',
      link: 'h3 a@href'
    },
    tags: [ 'Releaselog' ]
  },
  {
    url: 'http://www.pdfmagazines.org/page/',
    name: 'pdfmagazines',
    scope: '.item',
    schema: {
      title: 'h2 a',
      img: 'img@src',
      link: 'h2 a@href'
    },
    tags: [ 'pdfmagazines', 'magazine' ]
  },
  {
    url: 'http://allcomicbooks.us/page/',
    name: 'All Comic Books',
    scope: '.art-Post',
    schema: {
      title: 'h2 a',
      img: '.art-PostContent img@src',
      link: '.art-PostContent a@href'
    },
    post: function (item) {
      if (item && item.img) {
        item.img = item.img.replace('allcomicbooks.us//', '');
      }
    },
    tags: [ 'All Comic Books', 'comic book' ]
  },
  {
    url: 'http://worldmags.net/page/',
    name: 'WorldMags.net',
    scope: '.news-mags',
    schema: {
      title: '.news-title-mags',
      img: 'img@src',
      link: '.news-title-mags a@href'
    },
    tags: [ 'WorldMags.net', 'magazine' ]
  },
  {
    url: 'http://pdf-giant.com/page/',
    name: 'PDF giant',
    scope: '.mybox',
    schema: {
      title: 'h2 a',
      img: 'img@src',
      link: 'h2 a@href'
    },
    tags: [ 'PDF giant', 'magazine' ]
  },
  {
    url: 'http://avxhome.se/pages/',
    name: 'AvaxHome',
    scope: '.col-md-12.article',
    schema: {
      title: 'h1 a',
      img: 'img@src',
      link: 'h1 a@href'
    },
    post: function (item) {
      if (item.img) {
        item.img = item.img.replace('http://avxhome.se//', 'http://');
      }
    },
    tags: [ 'AvaxHome' ]
  },
  {
    url: 'http://www.bookgn.com/page/',
    name: 'Book GN',
    scope: '.module',
    schema: {
      title: '.title a',
      img: 'img@src',
      link: '.title a@href'
    },
    tags: [ 'Book GN', 'comics' ]
  },
  {
    url: 'http://pdf-magazine-download.com/page/',
    name: 'PDF Magazine Download',
    scope: '.PDFPerShort',
    schema: {
      title: 'a',
      img: 'img@src',
      link: 'a@href'
    },
    tags: [ 'PDF Magazine Downlaod', 'magazine' ]
  },
  {
    url: 'http://magazinesdownload.com/?page=',
    name: 'Magazines Download',
    scope: '.post',
    schema: {
      title: 'h1 a',
      img: 'img@src',
      link: 'h1 a@href'
    },
    tags: [ 'Magazines Download', 'magazine' ]
  },
  {
    url: 'http://www.israbox.eu/page/',
    name: 'IsraBox',
    scope: '.story',
    schema: {
      title: 'h2 a',
      img: '.content a img@src',
      link: 'h2 a@href'
    },
    tags: [ 'IsraBox', 'music' ]
  }
  //{
  //  url: 'http://www.1994hiphop.com/',
  //  name: '1994hiphop',
  //  schema: {
  //    $root: '',
  //    title: '',
  //    img: '',
  //    link: ''
  //  },
  //  tags: [ '1994hiphop', 'music' ]
  //},
  //{
  //  url: 'http://exystence.net/',
  //  name: 'exystence',
  //  schema: {
  //    $root: '',
  //    title: '',
  //    img: '',
  //    link: ''
  //  },
  //  tags: [ 'exystence', 'music' ]
  //},
  //{
  //  url: 'http://newalbumreleases.net/',
  //  name: 'New Album Releases',
  //  schema: {
  //    $root: '',
  //    title: '',
  //    img: '',
  //    link: ''
  //  },
  //  tags: [ 'New Album Releases', 'music' ]
  //}
];
