const express = require('express');
const fs = require('fs');
const _ = require('lodash');
const Q = require('q');
const open = require('open');
const sites = require('./sites');

const fingerprint = require('talisman/keyers/fingerprint').default;
const Xray = require('x-ray'); // https://github.com/lapwinglabs/x-ray

const config = require('./config');

let x = Xray();

// This uses the x-ray library to scrape a single site. It does a _fantastic_
// job of it and all you have to do is give it a simple schema describing what
// you want pulled and into which values it should be put. Look at the sites.js
// file to see the list of sites being scraped and what is pulled from each one.
function scrapeSite(site) {
  var scraped = Q.defer();

  console.log(`Scraping (${site.name})`);
  var xrayPromises = _.map(_.range(1, 30), function (page) {
    var d = Q.defer();

    x(site.url + page, site.scope, [ site.schema ])(function (err, array) {
        d.resolve(array);
      });

    return d.promise;
  });

  Q.all(xrayPromises).done(function (itemArrays) {
    scraped.resolve(_.map(_.flatten(itemArrays), function (item) {
      if (item && item.title) {
        item.title = item.title.trim();
        item.fingerprint = fingerprint(item.title);
        item.tags = site.tags;
        item.timestamp = Date.now();

        if (site.post) {
          site.post(item);
        }

        return item;
      } else {
        return { };
      }
    }));
  });

  return scraped.promise;
}

function scrapeAllSites() {
  var sitesScraped = [ ];
  var deferred = Q.defer();

  var sitesPromises = _.map(sites, function (site) {
    return scrapeSite(site).then(function (items) {
      console.log(`Scraping (${site.name}): ${items.length}`);
      sitesScraped.push({ name: site.name, numItems: items.length });

      return items;
    });
  });

  Q.all(sitesPromises).done(function (allItemArrays) {
    let outputFilename = 'app/js/allItems.json.js';
    let sortedItems = _.sortBy(_.flatten(allItemArrays), 'fingerprint');

    let jsonString = JSON.stringify(sortedItems, null, 2);

    fs.writeFile(outputFilename, `angular.module('Scrape').value('allItems', ${jsonString});`, function(err) {
      if (err) {
        console.log(err);
      } else {
        console.log("JSON saved to " + outputFilename);
      }
    });

    deferred.resolve(sitesScraped);
  });

  return deferred.promise;
}

var app = express();
var adminRouter = express.Router();

adminRouter.get('/refresh', function (req, res) {
  console.log('/refresh called');

  scrapeAllSites().then(function (results) {
    res.send(results);
  });
});

app.use(express.static(__dirname + '/app'));

app.use('/admin', adminRouter);

app.listen(config.port);
open('http://localhost:' + config.port);
